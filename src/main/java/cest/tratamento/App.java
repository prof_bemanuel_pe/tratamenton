package cest.tratamento;

import java.util.InputMismatchException;
import java.util.Scanner;

import javax.swing.JOptionPane;

import cest.tratamento.biblio.Biblioteca;
import cest.tratamento.biblio.Categoria;

/**
 * Hello world!
 *
 */
public class App {
	Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		App a = new App();
		a.menu();
	}

	private void menu() {
		do {
			System.out.println("###################");
			System.out.println("Escolha:");
			System.out.println("[1] - Cadastrar");
			System.out.println("[2] - Consultar");
			System.out.println("[3] - Listar");
			System.out.println("###################");
			try {
				switch (sc.nextInt()) {
				case 1:
					cadastrar();
					break;
				case 2:
					break;
				case 3:
					listar();
					break;
				}
			} catch (InputMismatchException e) {
				JOptionPane.showMessageDialog(null, e); //nao esq de importar
			} finally {
				sc.nextLine();
			}

		} while (true);
	}

	private void cadastrar() {
		Biblioteca b = Biblioteca.getInstance();
		System.out.println("Codigo:");
		int c = sc.nextInt();
		System.out.println("Nome:");
		sc.nextLine();
		String n = sc.nextLine();
		b.cadastrar(Categoria.LIVRO, c, n);
	}
	
	private void listar() {
		Biblioteca.getInstance().listar();
	}
}
