package cest.tratamento.biblio;

import java.util.ArrayList;

import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

public class Biblioteca {
	private static Biblioteca instance; 
	ArrayList<Item> acervo = new ArrayList<Item>();
	private Biblioteca() {
	}

	
	public static Biblioteca getInstance() {
		if (instance == null) instance = new Biblioteca();
		return instance;
	}
	
	public boolean cadastrar(Categoria cat, int c, String n) {
		Item i = null;
		switch (cat) {
		case LIVRO:
			i = new Livro(c,n);
			break;
		default:
			break;
		}
		return acervo.add(i);
	}
	
	public void listar() {
		JOptionPane.showMessageDialog(null,
				new JScrollPane(
					 new JList(
							 acervo.toArray()
					 )
					),
			     "Acervo", 
			     JOptionPane.INFORMATION_MESSAGE);
	}
}
