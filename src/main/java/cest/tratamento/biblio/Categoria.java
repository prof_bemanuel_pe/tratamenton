package cest.tratamento.biblio;

public enum Categoria {
	LIVRO, ANUARIO, REVISTA
}