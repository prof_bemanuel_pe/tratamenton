package cest.tratamento.biblio;

public interface Item {
	public Integer getCodigo();
	public String getNome();
	public String toString();
}
