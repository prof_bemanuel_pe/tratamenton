package cest.tratamento.biblio;

public class Livro implements Item {
	Integer codigo;
	String nome;

	public Livro(Integer c, String n) {
		this.codigo = c;
		this.nome = n;
	}

	@Override
	public Integer getCodigo() {
		return codigo;
	}

	@Override
	public String getNome() {
		return nome;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(String.format("%s - %s", codigo, nome));
		return sb.toString();
	}
}
